using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TestCanicula.PoolCore;

namespace TestCanicula
{
    /// <summary>
    /// ����� ����� � �����.
    /// ������������� ��������� ������ ��� ����������.
    /// </summary>

    public class Installer : MonoBehaviour
    {
        [SerializeField] Placeable prefab;
        [SerializeField] PlaceableSO[] placeables;

        private void Awake()
        {
            PoolContainer poolContainer = new PoolContainer();

            foreach (var item in placeables)
            {
                var placeableFactory = new Placeable.Factory(prefab, item);

                poolContainer.CreatePool<Placeable, Placeable.Pool>(placeableFactory, item.MaxCount);
            }

            PlacementController placeableController = new PlacementController(poolContainer, FindObjectsOfType<LocationArea>());
        }
    }
}
