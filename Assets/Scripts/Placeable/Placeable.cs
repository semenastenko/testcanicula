using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TestCanicula.PoolCore;
using UnityEngine.UI;

namespace TestCanicula
{
    /// <summary>
    /// ����� ����������� ����� ������ Placeable
    /// � �������� ���� ����� �������.
    /// </summary>
    public class Placeable : MonoBehaviour
    {
        public PlaceableSO Settings { get; private set; }

        /// <summary>
        /// ���������, ����������� ��������� IParamtable,
        /// ��� �������� ���������� � ���.
        /// </summary>
        public struct Params : IParamtable
        {
            public readonly Vector3 Position;

            public Params(Vector3 position)
            {
                Position = position;
            }

            public object[] GetAllParams() => new object[] { Position };
        }

        /// <summary>
        /// ������ "���" ��� �������� �������� ���� ��������.
        /// </summary>

        public class Pool : ObjectPool<Placeable>
        {
            public Placement Placement { get; private set; }
            protected override void OnCreated(Placeable item)
            {
                Placement = item.Settings.Placement;
                base.OnCreated(item);
            }

            protected override void OnSpawned(Placeable item, IParamtable param)
            {
                var currentParam = (Params)param;
                item.transform.position = currentParam.Position;

                base.OnSpawned(item, param);
            }

            protected override void OnDespawned(Placeable item)
            {
                base.OnDespawned(item);
            }
        }

        /// <summary>
        /// ������ "�������" ��� �������� �������� ���������� �������.
        /// </summary>
        public class Factory : Factory<Placeable>
        {
            private PlaceableSO _settings;
            private Transform _group;
            public Factory(Placeable item, PlaceableSO settings)
                : base(item)
            {
                _settings = settings;
                _group = new GameObject($"Container {_settings.name}").transform;
            }

            protected override Placeable OnCreate(Placeable item)
            {
                var placeable = GameObject.Instantiate(item, _group);
                placeable.Settings = _settings;

                placeable.name = _settings.name;
                placeable.GetComponent<Image>().sprite = _settings.Sprite;
                placeable.GetComponent<RectTransform>().sizeDelta = _settings.Sprite.rect.size * _settings.Scale;
                placeable.GetComponent<RectTransform>().pivot = _settings.Pivot;

                return placeable;
            }
        }
    }
}
