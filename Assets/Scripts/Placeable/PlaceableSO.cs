using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TestCanicula
{
    public enum OrderPriority
    {
        back, front
    }

    /// <summary>
    /// ScriptableObject, ����������� ����� ��������� 
    /// ��� ���������� ������� Placeable.
    /// </summary>
    [CreateAssetMenu(fileName = "Placeable", menuName = "SO/Placeable")]
    public class PlaceableSO : ScriptableObject
    {
        [SerializeField, Tooltip("Object image")] Sprite sprite;
        [SerializeField, Tooltip("Object location")] Placement placement;
        [SerializeField, Tooltip("Object size scale")] float scale = 1;
        [SerializeField, Tooltip("Object size scale")] Vector2 pivot = Vector2.one * 0.5f;
        [SerializeField, Tooltip("Object maximum possible number")] int maxCount = 1;
        [SerializeField, Tooltip("Object render order priority")] OrderPriority orderPriority;

        public Sprite Sprite => sprite;
        public Placement Placement => placement;
        public float Scale => scale;
        public Vector2 Pivot => pivot;
        public int MaxCount => maxCount;
        public OrderPriority OrderPriority => orderPriority;
    }
}
