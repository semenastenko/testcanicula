﻿using System;
using System.Linq;
using System.Collections.Generic;
using TestCanicula.PoolCore;
using UnityEngine;
using Random = UnityEngine.Random;

namespace TestCanicula
{
    /// <summary>
    /// Контроллер размещения объектов Placeable.
    /// Размещает из пула объект в указанную зону.
    /// </summary>
    public class PlaceableController
    {
        private PoolContainer _poolContainer;
        private Queue<(Placeable item, Placeable.Pool pool)> spawned;

        public PlaceableController(PoolContainer poolContainer)
        {
            _poolContainer = poolContainer;
            spawned = new Queue<(Placeable placeable, Placeable.Pool pool)>();
        }

        /// <summary>
        /// При успешном получении доступного рандомного пула, 
        /// размещает объект в указанную зону.
        /// </summary>
        public bool TryPlace(Vector3 mousePosition, LocationArea locationArea)
        {
            Placeable.Pool pool = GetRandomPool(locationArea.GetPlacement()) as Placeable.Pool;
            if (pool == null) return false;

            var item = pool.Spawn(new Placeable.Params(mousePosition));
            item.transform.SetParent(locationArea.transform);

            spawned.Enqueue((item, pool));

            UpdateSiblingIndexes(locationArea);

            return true;
        }

        /// <summary>
        /// Возвращает все объекты обратно в пул.
        /// </summary>
        public void Reset()
        {
            while (spawned.Count != 0)
            {
                var element = spawned.Dequeue();
                element.pool.Despawn(element.item);
            }
        }

        /// <summary>
        /// Получает все размещенные объекты в указанной зоне.
        /// </summary>
        public Placeable[] GetLocatedPlaceables(LocationArea locationArea)
        {
            List<Placeable> placeables = new List<Placeable>();
            foreach (var spawn in spawned)
            {
                if (spawn.item.transform.parent == locationArea.transform)
                {
                    placeables.Add(spawn.item);
                }
            }
            return placeables.ToArray();
        }

        private IPool[] GetPools(Placement placement) =>
            _poolContainer.GetPools<Placeable.Pool>().FindAll(x => x.Placement == placement).ToArray();

        private IPool GetRandomPool(Placement placement)
        {
            var randomPool = new List<IPool>();
            foreach (var pool in GetPools(placement))
            {
                if (pool.InactiveCount > 0)
                {
                    randomPool.Add(pool);
                }
            }
            if (randomPool.Count == 0) return null;

            int randomIndex = Random.Range(0, randomPool.Count);
            return randomPool[randomIndex];
        }

        private void UpdateSiblingIndexes(LocationArea locationArea)
        {
            List<Placeable> placeables = new List<Placeable>(GetLocatedPlaceables(locationArea));

            placeables.Sort((left, right) => left.transform.position.y.CompareTo(right.transform.position.y));

            int orderOffset = 0;
            foreach (var placeable in placeables)
            {
                if (placeable.Settings.OrderPriority == OrderPriority.back)
                {
                    orderOffset++;
                    placeable.transform.SetSiblingIndex(0);
                }
                else
                {
                    placeable.transform.SetSiblingIndex(orderOffset);
                }
            }
        }

    }
}