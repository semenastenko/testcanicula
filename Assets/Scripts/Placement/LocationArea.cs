using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace TestCanicula
{
    /// <summary>
    /// ����������� �����, ����������� ���� ���������� �������� �� �����.
    /// </summary>
    public abstract class LocationArea : MonoBehaviour, IPointerEnterHandler
    {
        public event Action<LocationArea> OnLocationChange;
        
        public void OnPointerEnter(PointerEventData eventData)
        {
            OnLocationChange?.Invoke(this);
        }

        public abstract Placement GetPlacement();
    }
}
