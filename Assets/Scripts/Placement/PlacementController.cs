using System.Collections;
using System.Collections.Generic;
using TestCanicula.PoolCore;
using UnityEngine;
using UniRx;
using System;

namespace TestCanicula
{
    /// <summary>
    /// ����������, ������������ ������� ���� ��� ���������� ��������.
    /// </summary>
    public class PlacementController : IDisposable
    {
        private PlaceableController _placeableController;
        private LocationArea[] _locations;

        private IDisposable _updateDispose;

        public LocationArea CurrentLocationArea { get; private set; }

        public PlacementController(PoolContainer poolContainer, LocationArea[] locations)
        {
            _placeableController = new PlaceableController(poolContainer);

            _locations = locations;
            foreach (var location in _locations)
            {
                location.OnLocationChange += ChangePlacement;
            }

            _updateDispose = Observable.EveryUpdate().Subscribe(_ => Update());
        }

        private void ChangePlacement(LocationArea locationArea)
        {
            CurrentLocationArea = locationArea;
        }

        private void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                var isSpawn = _placeableController.TryPlace(Input.mousePosition, CurrentLocationArea);
            }
            if (Input.GetMouseButtonDown(1))
            {
                _placeableController.Reset();
            }
        }

        public void Dispose()
        {
            foreach (var location in _locations)
            {
                location.OnLocationChange -= ChangePlacement;
            }
            _updateDispose?.Dispose();
        }
    }
}
