using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TestCanicula
{
    public class SkyArea : LocationArea
    {
        public override Placement GetPlacement() => Placement.sky;
    }
}
