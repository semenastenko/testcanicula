using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TestCanicula
{
    public class SeaArea : LocationArea
    {
        public override Placement GetPlacement() => Placement.sea;
    }
}
