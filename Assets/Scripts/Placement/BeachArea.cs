using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TestCanicula
{
    public class BeachArea : LocationArea
    {
        public override Placement GetPlacement() => Placement.beach;
    }
}
